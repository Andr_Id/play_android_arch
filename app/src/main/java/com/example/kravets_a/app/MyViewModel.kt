package com.example.kravets_a.app

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import com.example.kravets_a.app.model.User
import com.example.kravets_a.app.model.UserRepo
import io.reactivex.observers.DisposableSingleObserver
import io.reactivex.schedulers.Schedulers
import java.util.concurrent.TimeUnit

class MyViewModel constructor(private val userRepo: UserRepo) : ViewModel() {

    private var userLiveData: MutableLiveData<User> = MutableLiveData()
    private var progressLiveData: MutableLiveData<Boolean> = MutableLiveData()

    fun getUser(): LiveData<User> {
        loadUsers()
        return userLiveData
    }

    fun getProgress(): LiveData<Boolean> = progressLiveData

    private fun loadUsers() {
        progressLiveData.value = true
        userRepo.load()
                .delay(4, TimeUnit.SECONDS)
                .subscribeOn(Schedulers.io())
                .subscribe(object : DisposableSingleObserver<User>() {
                    override fun onSuccess(t: User) {
                        userLiveData.postValue(t)
                        progressLiveData.postValue(false)
                    }

                    override fun onError(e: Throwable) {

                    }
                })
    }
}