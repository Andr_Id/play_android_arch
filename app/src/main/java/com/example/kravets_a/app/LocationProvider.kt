package com.example.kravets_a.app

import android.arch.lifecycle.Lifecycle
import android.arch.lifecycle.LifecycleObserver
import android.arch.lifecycle.OnLifecycleEvent
import android.os.Handler


class LocationProvider(private val callback: Callback,
                       lifecycle: Lifecycle) : LifecycleObserver {

    private var handler: Handler = Handler()

    init {
        lifecycle.addObserver(this)
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_START)
    fun start() {
        handler.postDelayed({
            callback.provideLocation("Lviv")
        }, 6000)
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
    fun stop() {
        handler.removeCallbacksAndMessages(null)
    }
}

interface Callback {
    fun provideLocation(location: String)
}