package com.example.kravets_a.app

import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider

import com.example.kravets_a.app.model.UserRepo

class UserViewModelFactory
constructor(private val userRepo: UserRepo) : ViewModelProvider.Factory {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return MyViewModel(userRepo) as T
    }
}