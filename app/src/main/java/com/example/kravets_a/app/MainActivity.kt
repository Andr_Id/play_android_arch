package com.example.kravets_a.app

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import com.example.kravets_a.app.model.User
import com.example.kravets_a.app.model.UserRepo
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity(), Callback {

    private lateinit var locationProvider: LocationProvider

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        locationProvider = LocationProvider(this, lifecycle)
        stopLocationButton.setOnClickListener { locationProvider.stop() }

        val userRepo = UserRepo()
        val factory = UserViewModelFactory(userRepo)
        val model = ViewModelProviders.of(this, factory).get(MyViewModel::class.java)

        model.getUser().observe(this, Observer<User> { updateUi(it) })
        model.getProgress().observe(this, Observer<Boolean> { updateProgress(it) })
    }

    override fun provideLocation(location: String) {
        locationTextView.text = location
    }

    private fun updateProgress(isActive: Boolean?) {
        if (isActive!!) progressBar.visibility = View.VISIBLE
        else progressBar.visibility = View.GONE
    }

    private fun updateUi(user: User?) {
        firstName.text = user?.firstName
        lastName.text = user?.lastName
    }
}
