package com.example.kravets_a.app.model

import io.reactivex.Single

class UserRepo{

    private var counter = 0

    fun load(): Single<User> {
        val user = User("ss", counter++.toString())
        return Single.just(user)
    }
}

data class User(var firstName: String = "",
                var lastName: String = "")